#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""Clase (y programa principal) para un servidor de eco en UDP simple."""

import socketserver
import sys
import time
import json


class SIPRegisterHandler(socketserver.DatagramRequestHandler):
    """SIP Register server class."""

    dic = {}

    def register2json(self):
        """Convert dictionary to json."""
        with open("registered.json", "w") as jsonfile:
            json.dump(self.dic, jsonfile, indent=4)

    def json2registered(self):
        """Read a json file and use the info as a dictionary of users."""
        try:
            with open("registered.json", "r") as jsonfile:
                self.dic = json.load(jsonfile)
        except FileNotFoundError:
            pass

    def handle(self):
        """
        Handle method of the server class.
        (all requests will be handled by this method).
        """
        if self.dic == {}:
            self.json2registered()

        IP = self.client_address[0]
        C_PORT = self.client_address[1]
        print("IP:" + str(IP) + " Port:" + str(C_PORT))

        line = self.rfile.read()
        print(line.decode("utf-8"))
        MESSAGE = line.decode("utf-8").split()
        USER = MESSAGE[1].split(":")[1]
        expired = int(MESSAGE[-1])

        if MESSAGE[0] == "REGISTER":
            if expired == 0:
                if USER in self.dic:
                    del self.dic[USER]
                    self.wfile.write(b"SIP/2.0 200 OK\r\n\r\n")
            else:
                expired += time.time()
                del_time = time.strftime("%Y-%m-%d %H:%M:%S",
                                         time.localtime(expired))
                self.dic[USER] = [str(IP), str(del_time)]
                self.wfile.write(b"SIP/2.0 200 OK\r\n\r\n")

            lista = []
            current_time = time.localtime(time.time())

            for name in self.dic:
                del_time = time.strptime(self.dic[name][1],
                                         "%Y-%m-%d %H:%M:%S")
                if current_time >= del_time:
                    lista.append(name)
            for name in lista:
                del self.dic[name]
            self.wfile.write(b"SIP/2.0 200 OK\r\n\r\n")
            print(self.dic)
            print("\r\n")
            self.register2json()

        else:
            sys.exit("SIP/2.0 400 SOLICITUD ERRONEA")


if __name__ == "__main__":
    """
    Listens at localhost ('') port given
    and calls the SIPRegisterHandler class to manage the request.
    """
    try:
        PORT = int(sys.argv[1])
    except IndexError or ValueError:
        sys.exit("Usage: server.py puerto")
    serv = socketserver.UDPServer(('', PORT), SIPRegisterHandler)
    print("Lanzando servidor UDP...")
    try:
        serv.serve_forever()
    except KeyboardInterrupt:
        print(" Finalizado servidor")
