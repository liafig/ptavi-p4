#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""Programa cliente UDP que abre un socket a un servidor."""

import socket
import sys

# Constantes. Dirección IP del servidor y contenido a enviar

try:
    SERVER = sys.argv[1]
    PORT = int(sys.argv[2])
    USER = sys.argv[4]
    EXPIRES = int(sys.argv[5])
except IndexError or ValueError:
    sys.exit("Usage: client.py ip puerto REGISTER sip_address expires_value")

if EXPIRES < 0:
    sys.exit("Expiration value must be greater than 0\r\n\r\n")

# Creamos el socket, lo configuramos y lo atamos a un servidor/puerto

with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as my_socket:
    my_socket.connect((SERVER, PORT))
    if sys.argv[3] == "REGISTER":
        USER_CONTENT = "REGISTER sip:" + USER + " SIP/2.0\r\n"
        EXP_CONTENT = "Expires: " + str(EXPIRES) + "\r\n\r\n"
        INFO = USER_CONTENT + EXP_CONTENT
        my_socket.send(bytes(INFO, "utf-8"))
        data = my_socket.recv(1024)
        print('Recibido -- ', data.decode("utf-8"))

    else:
        sys.exit("Missing REGISTER")

print("Socket terminado.")
